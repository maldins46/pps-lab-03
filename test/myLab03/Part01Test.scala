package myLab03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Part01Test {

  import u03.Lists.List._
  import myLab03.Part01._

  val lst: Cons[Int] = Cons(10,Cons(20, Cons(30, Nil())))

  @Test def testDrop(){
    assertEquals(Cons(20,Cons(30, Nil())), drop(lst,1))
    assertEquals(Cons(30, Nil()), drop(lst,2))
    assertEquals(Nil(), drop(lst ,5))
  }

  @Test def testFlatMap(){
    assertEquals(Cons(11,Cons(21,Cons(31,Nil()))), flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil())))))), flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMapWithFlatMap(): Unit = {
    assertEquals(Cons(11,Cons(21,Cons(31,Nil()))), mapWithFlatMap(lst)(v => v+1))
    assertEquals(Cons(12,Cons(22,Cons(32,Nil()))), mapWithFlatMap(lst)(v => v+2))
  }

  @Test def testFilterWithFlatMap(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), filterWithFlatMap(lst)(v => v >= 20))
    assertEquals(Cons(30,Nil()), filterWithFlatMap(lst)(v => v == 30))
  }

  @Test def testMax(): Unit = {
    import u02.Optionals.Option._

    assertEquals(Some(30), max(lst))
    assertEquals(Cons(30,Nil()), filterWithFlatMap(lst)(v => v == 30))
  }
}