package myLab03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Part02Test {

  import u02.Modules.Person._
  import u03.Lists.List._
  import myLab03.Part02._

  @Test def testGetCourses() {
    assertEquals(Cons("English",Cons("Math", Nil())), getCourses(Cons(Student("Matteo",2015), Cons(Teacher("Riccardo","English"), Cons(Student("Francesco",2018), Cons(Teacher("Lorenzo", "Math"), Nil()))))))
  }

  @Test def testFoldLeft(): Unit = {
    val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

    assertEquals(-16, foldLeft(lst)(0)(_-_))
  }

  @Test def testFoldRight(): Unit = {
    val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

    assertEquals(Cons(5,Cons(1,Cons(7,Cons(3, Nil())))), reverse(lst)) // OK

    assertEquals(-8, foldRight(lst)(0)(_-_)) // not working
  }
}