package myLab03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Part03Test {

  import u03.Lists.List._
  import u03.Streams._
  import Part03._


  @Test def testDrop() {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)

    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))), Stream.toList(drop(s)(6)))
  }

  @Test def testConstant(): Unit = {
    val expectedRes = Cons("x",Cons("x",Cons("x",Cons("x",Cons("x",Nil())))))
    val constantRes = Stream.toList(Stream.take(constant("x"))(5))

    assertEquals(expectedRes, constantRes)
  }

  @Test def fibTest(): Unit = {
    val expectedRes = Cons(0,Cons(1,Cons(1,Cons(2,Cons(3,Cons(5,Cons(8,Cons(13,Nil()))))))))
    val fibTakeRes = Stream.toList(Stream.take(fibs)(8))

    assertEquals(expectedRes, fibTakeRes)
  }
}