package myLab03


object Part02 {
  import Part01._
  import u03.Lists.List._
  import u03.Lists._
  import u02.Modules.Person
  import u02.Modules.Person._

  def getCourses(l: List[Person]): List[String] = flatMap(l) {
    case Teacher(_, c) => Cons(c, Nil())
    case _ => Nil()
  }

  @scala.annotation.tailrec
  def foldLeft[A](l: List[A])(acc: A)(op: (A, A) => A): A = l match {
    case Nil() => acc
    case Cons(h, t) => foldLeft(t)(op(acc, h))(op)
  }

  def foldRight[A](l: List[A])(acc: A)(op: (A, A) => A): A = foldLeft(reverse(l))(acc)(op)

  def reverse[A](l: List[A]): List[A] = l match {
    case Nil() => Nil()
    case Cons(h, Nil()) => Cons(h, Nil())
    case Cons(_, t) => Cons(getLast(t), reverse(listWithoutLast(l)))
  }

  @scala.annotation.tailrec
  def getLast[A](l: List[A]): A = l match {
    case Cons(h, Nil()) => h
    case Cons(h,t) => getLast(t)
  }

  def listWithoutLast[A](l: List[A]): List[A] = l match {
    case Nil() => Nil()
    case Cons(_, Nil()) => Nil()
    case Cons(h,t) => Cons(h, listWithoutLast(t))
  }
}
