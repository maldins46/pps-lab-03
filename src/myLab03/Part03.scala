package myLab03


object Part03 {

  import u03.Streams._
  import u03.Streams.Stream._


  def drop[A](stream: Stream[A])(n: Int): Stream[A] = (stream, n) match {
    case (Cons(_, tail), n) if n > 0 => drop(tail())(n - 1)
    case (a,_) => a
  }

  def constant[A](value:A): Stream[A] = cons(value, constant(value))


  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n - 1) + fib(n - 2)
  }

  val fibs: Stream[Int] = map(iterate(0)(_+1))(fib)
}
