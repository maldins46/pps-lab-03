package myLab03

import u03.Lists

object Part01 {
  import u03.Lists.List._
  import u03.Lists._
  import u02.Optionals.Option._
  import u02.Optionals._

  @scala.annotation.tailrec
  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(_, t), n) if n > 1 => drop(t, n - 1)
    case (Cons(_, t), 1) => t
    case (Nil(), _) => Nil()
  }


  def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
    case Cons(h,t) => append(f(h), flatMap(t)(f))
    case Nil() => Nil()
  }


  def mapWithFlatMap[A,B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(v => Cons(mapper(v), Nil()))

  def filterWithFlatMap[A](l1: List[A])(pred: A => Boolean): List[A] = flatMap(l1)(v => pred(v) match {
    case true => Cons(v, Nil())
    case _ => Nil()
  })

  def max(l: List[Int]): Option[Int] = l match  {
    case Cons(t,h) => getHigher(Some(t), max(h))
    case Nil() => None()
  }

  def getHigher(a: Option[Int], b: Option[Int]): Option[Int] = (a,b) match {
    case (None(), Some(b)) => Some(b)
    case (Some(a), None()) => Some(a)
    case (Some(a), Some(b)) if a >= b => Some(a)
    case (Some(a), Some(b)) if a < b => Some(b)
    case (None(), None()) => None()
  }
}
